import axios from '@/plugins/axios'

const state = {
  forecastDays: {}
}

const mutations = {
  SET_CURRENT_FORECAST_DAYS (state, data) {
    state.forecastDays = data
  }
}

const actions = {
  getForecastDays (context, arg) {
    return axios.get('/forecast.json', {
      params: {
        q: arg.str,
        days: arg.numberOfDays
      }
    })
  }
}

const getters = {
  forecastDays: (state) => {
    return state.forecastDays.forecast
      ? state.forecastDays.forecast.forecastday
      : []
  },
  forecastDay: (state) => {
    return (state.forecastDays.location && state.forecastDays.current)
      ? { ...state.forecastDays.location, ...state.forecastDays.current }
      : null
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
