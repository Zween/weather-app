const state = {
  cities: []
}
const mutations = {
  ADD (state, data) {
    const index = state.cities.findIndex(city => city.id === data.id)
    if (index === -1) {
      state.cities.push(data)
    }
  },
  DELETE (state, data) {
    const index = state.cities.findIndex(city => city.id === data.id)
    if (index > -1) {
      state.cities.splice(index, 1)
    }
  }
}

const actions = {}

const getters = {
  isFavorite: (state) => (data) => state.cities.map(city => city.id).includes(data)
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
