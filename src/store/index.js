import Vue from 'vue'
import Vuex from 'vuex'
import i18n from '@/plugins/i18n'
import vuetify from '@/plugins/vuetify'
import forecast from './modules/forecast'
import favorites from './modules/favorites'
import axios from '@/plugins/axios'
import dayjs from 'dayjs'
import 'dayjs/locale/fr'
import 'dayjs/locale/en'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState({
    key: 'weatherApp',
    paths: [
      'favorites.cities',
      'forecast.forecastDays',
      'celsiusTemperature',
      'windInKm',
      'language'
    ]
  })],
  modules: {
    forecast,
    favorites,
    snackbar: {}
  },
  state: {
    cities: [],
    celsiusTemperature: true,
    language: '',
    windInKm: true
  },
  mutations: {
    SET_APP_LANGUAGE (state, data) {
      dayjs.locale(data)
      i18n.locale = data
      vuetify.framework.lang.current = data
      state.language = data
    },
    SET_CITIES (state, data) {
      state.cities = data
    },
    SET_SNACKBAR (state, data) {
      state.snackbar = data
    },
    SET_TYPE_OF_TEMPERATURE (state, data) {
      state.celsiusTemperature = data
    }
  },
  actions: {
    searchCity (context, str) {
      return axios.get('/search.json', {
        params: {
          q: str
        }
      })
    }
  }
})
