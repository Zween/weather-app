'use strict'
import axios from 'axios'
import store from '@/store/'

const config = {
  baseURL: process.env.VUE_APP_WEATHER_API_URL
}

const _axios = axios.create(config)

_axios.interceptors.request.use(
  function (config) {
    config.params.key = process.env.VUE_APP_WEATHER_API_KEY
    const lang = store.state.language
    if (lang === 'fr') { config.params.lang = lang }
    return config
  },
  function (error) {
    console.error('Error api request from interceptor request: ', error)
    return Promise.reject(error)
  }
)

_axios.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    const snackbar = {
      color: 'red',
      text: 'Error from Weather Api',
      timeout: 6000
    }
    store.commit('SET_SNACKBAR', snackbar)
    console.error('Error api request from interceptor response: ', error)
    return Promise.reject(error)
  }
)

export default _axios
