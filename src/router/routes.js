const Forecast = () => import(/* webpackChunkName: "Forecast" */ '../views/Forecast.vue')
const Favorites = () => import(/* webpackChunkName: "Favorites" */ '../views/Favorites.vue')
const Page404 = () => import(/* webpackChunkName: "Page404" */ '../views/Page404.vue')

const routes = [
  {
    path: '*',
    redirect: '/404'
  },
  {
    path: '/',
    redirect: '/forecast'
  },
  {
    path: '/forecast/:cityId?',
    name: 'Forecast',
    component: Forecast
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites
  },
  {
    path: '/404',
    name: '404',
    component: Page404
  }
]

export default routes
